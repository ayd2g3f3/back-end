import "reflect-metadata";
import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
const bodyParser = require('body-parser');

//import userRoutes from './routes/user.routes'
import appRoutes from './routes/app.routes';

// Destructure our bodyParser methods
const { urlencoded, json } = bodyParser;
const port = process.env.PORT || 9000;


const app = express();

// Parse incoming requests data (https://github.com/expressjs/body-parser)
app.use(json());
app.use(urlencoded({ extended: false }));

// Middlewares
app.use(cors());
app.use(express.json());
app.use(morgan('dev'));

// routes
app.use(appRoutes);

app.listen(port);
console.log(`Express server listening on http://localhost:${port}`);

module.exports = app;