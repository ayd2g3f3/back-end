import { Request, Response } from "express";
import { connection } from "../connection/Connection";
import { Usuario } from "../entity/Usuario";

export const postAgregarUsuario = async (
  req: Request,
  res: Response
): Promise<Response> => {
  return connection
    .then(async (connection) => {
        const usuario = new Usuario();
        usuario.correo = req.body.correo;
        usuario.nombre = req.body.nombre;
        usuario.password = req.body.password;
        await connection.manager.save(usuario);
      return res.status(200).send();
    })
    .catch((error) => {
      return res.status(404).json(error).send();
    });
};
