import { Request, Response } from "express";
import { connection } from "../connection/Connection";

export const getInicio = async (
  req: Request,
  res: Response
): Promise<Response> => {
  return connection
    .then(async (connection) => {
      return res.status(200).send();
    })
    .catch((error) => {
      return res.status(404).json(error).send();
    });
};
