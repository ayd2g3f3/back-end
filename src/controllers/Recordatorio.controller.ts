import { Request, Response } from "express";
import { connection } from "../connection/Connection";
import { Recordatorio } from "../entity/Recordatorio";

export const getRecordatorios = async (
  req: Request,
  res: Response
): Promise<Response> => {
  return connection
    .then(async (connection) => {
      const recordatorioRepository = connection.getRepository(Recordatorio);
      const recordatorios = await recordatorioRepository.find({
        where: { usuario: req.body.usuario },
      });
      return res.status(200).json(recordatorios).send();
    })
    .catch((error) => {
      return res.status(404).json(error).send();
    });
};

export const postAgregarRecordatorio = async (
  req: Request,
  res: Response
): Promise<Response> => {
  return connection
    .then(async (connection) => {
      const recordatorio = new Recordatorio();
      recordatorio.titulo = req.body.titulo;
      recordatorio.descripcion = req.body.descripcion;
      recordatorio.usuario = req.body.usuario;

      await connection.manager.save(recordatorio);

      return res.status(200).send();
    })
    .catch((error) => {
      return res.status(404).json(error).send();
    });
};
