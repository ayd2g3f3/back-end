const supertest = require("supertest");
const app = require("../index");
const chai = require("chai");

chai.should();

const api = supertest.agent(app);

describe("/get inicio", () => {
  it("should connect to the app", (done) => {
    api
      .get("/inicio")
      .end((err: any, res: any) => {
        res.status.should.equal(200);
        done();
      });
  });
});

describe("/post agregarUsuario", () => {
  it("should add a new user", (done) => {
    api
      .post("/agregarUsuario")
      .send({
        "correo" : "usuario2@gmail.com",
        "nombre" : "usuario2",
        "password" : "usuario2"
      })
      .end((err: any, res: any) => {
        res.status.should.equal(200);
        done();
      });
  });
});