import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from "typeorm";
import { Usuario } from "./Usuario";

@Entity()
export class Nota {
  @PrimaryGeneratedColumn()
  idNota: number;

  @Column()
  titulo: string;

  @Column()
  descripcion: string;

  @Column({ default: new Date() })
  fecha: Date;
    
  @ManyToOne((type) => Usuario, (usuario) => usuario.notas)
  usuario: Usuario;

}
