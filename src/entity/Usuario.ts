import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from "typeorm";
import { Alarma } from "./Alarma";
import { Nota } from "./Nota";
import { Recordatorio } from "./Recordatorio";


@Entity()
export class Usuario {
  @PrimaryGeneratedColumn()
  idUsuario: number;

  @Column()
  nombre: string;

  @Column()
  password: string;

  @Column()
  correo: string;

  @OneToMany((type) => Alarma, (alarma) => alarma.usuario)
  alarmas: Alarma[];

  @OneToMany((type) => Nota, (nota) => nota.usuario)
  notas: Nota[];

  @OneToMany((type) => Recordatorio, (recordatorio) => recordatorio.usuario)
  recordatorios: Recordatorio[];

}
