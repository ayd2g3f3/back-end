import { Router } from "express";
const router = Router();

import { getInicio } from "../controllers/inicio.controller";
import { postAgregarUsuario } from "../controllers/Usuario.controller";
import { postAgregarRecordatorio } from "../controllers/Recordatorio.controller";
import { getRecordatorios } from "../controllers/Recordatorio.controller";

router.get("/inicio", getInicio);
router.get("/recordatorios", getRecordatorios);
router.post("/agregarUsuario", postAgregarUsuario);
router.post("/agregarRecordatorio", postAgregarRecordatorio);

export default router;
