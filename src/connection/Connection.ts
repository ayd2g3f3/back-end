import { createConnection } from "typeorm";

export const connection = createConnection({
  type: "postgres",
  host: "35.192.67.129",
  port: 5432,
  username: "fase3",
  password: "fase3",
  database: "fase3",
  entities: ["dist/entity/**/*.js"],
  synchronize: true,
  logging: false,
});
