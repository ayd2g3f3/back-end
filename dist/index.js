"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
var express_1 = __importDefault(require("express"));
var morgan_1 = __importDefault(require("morgan"));
var cors_1 = __importDefault(require("cors"));
var bodyParser = require('body-parser');
//import userRoutes from './routes/user.routes'
var app_routes_1 = __importDefault(require("./routes/app.routes"));
// Destructure our bodyParser methods
var urlencoded = bodyParser.urlencoded, json = bodyParser.json;
var port = process.env.PORT || 9000;
var app = express_1.default();
// Parse incoming requests data (https://github.com/expressjs/body-parser)
app.use(json());
app.use(urlencoded({ extended: false }));
// Middlewares
app.use(cors_1.default());
app.use(express_1.default.json());
app.use(morgan_1.default('dev'));
// routes
app.use(app_routes_1.default);
app.listen(port);
console.log("Express server listening on http://localhost:" + port);
module.exports = app;
