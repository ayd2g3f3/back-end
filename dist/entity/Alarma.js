"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var typeorm_1 = require("typeorm");
var Usuario_1 = require("./Usuario");
var Alarma = /** @class */ (function () {
    function Alarma() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn(),
        __metadata("design:type", Number)
    ], Alarma.prototype, "idAlarma", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Alarma.prototype, "titulo", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Alarma.prototype, "descripcion", void 0);
    __decorate([
        typeorm_1.Column({ default: new Date() }),
        __metadata("design:type", Date)
    ], Alarma.prototype, "fecha", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Usuario_1.Usuario; }, function (usuario) { return usuario.alarmas; }),
        __metadata("design:type", Usuario_1.Usuario)
    ], Alarma.prototype, "usuario", void 0);
    Alarma = __decorate([
        typeorm_1.Entity()
    ], Alarma);
    return Alarma;
}());
exports.Alarma = Alarma;
