"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var typeorm_1 = require("typeorm");
var Alarma_1 = require("./Alarma");
var Nota_1 = require("./Nota");
var Recordatorio_1 = require("./Recordatorio");
var Usuario = /** @class */ (function () {
    function Usuario() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn(),
        __metadata("design:type", Number)
    ], Usuario.prototype, "idUsuario", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Usuario.prototype, "nombre", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Usuario.prototype, "password", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Usuario.prototype, "correo", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return Alarma_1.Alarma; }, function (alarma) { return alarma.usuario; }),
        __metadata("design:type", Array)
    ], Usuario.prototype, "alarmas", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return Nota_1.Nota; }, function (nota) { return nota.usuario; }),
        __metadata("design:type", Array)
    ], Usuario.prototype, "notas", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return Recordatorio_1.Recordatorio; }, function (recordatorio) { return recordatorio.usuario; }),
        __metadata("design:type", Array)
    ], Usuario.prototype, "recordatorios", void 0);
    Usuario = __decorate([
        typeorm_1.Entity()
    ], Usuario);
    return Usuario;
}());
exports.Usuario = Usuario;
