"use strict";
var supertest = require("supertest");
var app = require("../index");
var chai = require("chai");
chai.should();
var api = supertest.agent(app);
describe("/get inicio", function () {
    it("should connect to the app", function (done) {
        api
            .get("/inicio")
            .end(function (err, res) {
            res.status.should.equal(200);
            done();
        });
    });
});
describe("/post agregarUsuario", function () {
    it("should add a new user", function (done) {
        api
            .post("/agregarUsuario")
            .send({
            "correo": "usuario2@gmail.com",
            "nombre": "usuario2",
            "password": "usuario2"
        })
            .end(function (err, res) {
            res.status.should.equal(200);
            done();
        });
    });
});
